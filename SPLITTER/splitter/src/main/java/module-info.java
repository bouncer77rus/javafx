module ru.t1consulting.nsitwo.utils.splitter {
    requires javafx.controls;
    requires javafx.fxml;
    requires org.apache.logging.log4j;
    requires commons.csv;
    requires org.apache.commons.io;
    requires org.apache.poi.ooxml;

    opens ru.t1consulting.nsitwo.utils.splitter to javafx.fxml;
    exports ru.t1consulting.nsitwo.utils.splitter;
}