package ru.t1consulting.nsitwo.utils.splitter;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

public class SplitterApplication extends Application {

    private static final Logger log = LogManager.getLogger(SplitterApplication.class);

    public static Stage primaryStage;
    @Override
    public void start(Stage primaryStage) {
        log.info("Start Splitter Application on JavaFX");
        FXMLLoader fxmlLoader = new FXMLLoader(SplitterApplication.class.getResource("application2.fxml"));
        Scene scene = null;
        try {
            scene = new Scene(fxmlLoader.load(), 900, 500);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }



        primaryStage.setTitle("Splitter");
        primaryStage.setScene(scene);
        primaryStage.getScene().getStylesheets().add("JMetroDarkTheme.css");
        SplitterApplication.primaryStage = primaryStage; // Для смены темы в контроллере

        SplitterController splitterController = fxmlLoader.getController();
        splitterController.setPrimaryStage(primaryStage);

        primaryStage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}