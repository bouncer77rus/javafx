package ru.t1consulting.nsitwo.utils.splitter;

import javafx.beans.binding.StringBinding;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ToggleButton;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.util.ListResourceBundle;
import java.util.Locale;
import java.util.ResourceBundle;

public class SplitterController implements Initializable {

    private static final String RESOURCE_NAME = Resources.class.getTypeName();

    private static final ObservableResourceFactory RESOURCE_FACTORY = new ObservableResourceFactory();
    private static final Logger log = LogManager.getLogger(SplitterController.class);

    static {
        RESOURCE_FACTORY.setResources(ResourceBundle.getBundle(RESOURCE_NAME));
    }

    private Stage primaryStage;

    @FXML
    private Label themeLabel;

    @FXML
    private ComboBox<Locale> languageSelect = new ComboBox<>();

    @FXML
    private Label languageLabel;

    @FXML
    private Label welcomeText;

    @FXML
    private ToggleButton toggleTheme;

    @FXML
    protected void onHelloButtonClick() {
        welcomeText.setText("Welcome to JavaFX Application!");
    }

    @FXML
    private Label langLabel;

    @FXML
    protected void onSwitchLang() {

        languageSelect.valueProperty().addListener((obs, oldValue, newValue) -> {
            System.out.println("Change Lang! " + oldValue + " to " + newValue);
            if (newValue != null) {
                RESOURCE_FACTORY.setResources(ResourceBundle.getBundle(RESOURCE_NAME, newValue));
            }
        });


    }

    @FXML
    protected void onThemeButtonClick() {

        if (toggleTheme.isSelected()) {
            themeLabel.setText("Theme Light");
            primaryStage.getScene().getStylesheets().remove("JMetroDarkTheme.css");
            primaryStage.getScene().getStylesheets().add("JMetroLightTheme.css");
        } else {
            themeLabel.setText("Theme Dark");
            primaryStage.getScene().getStylesheets().remove("JMetroLightTheme.css");
            primaryStage.getScene().getStylesheets().add("JMetroDarkTheme.css");
        }

        log.info(primaryStage.getScene().getStylesheets());
    }

    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        Locale rus = new Locale("ru", "RU");
        languageSelect.getItems().removeAll(languageSelect.getItems());
        languageSelect.getItems().addAll(rus, Locale.ENGLISH, Locale.FRENCH);
        languageSelect.getSelectionModel().select(rus);
        languageSelect.setCellFactory(lv -> new LocaleCell());
        languageSelect.setButtonCell(new LocaleCell());
        languageLabel.setText(languageSelect.getPromptText());

        langLabel.textProperty().bind(RESOURCE_FACTORY.getStringBinding("greeting"));
    }

    public static class LocaleCell extends ListCell<Locale> {
        @Override
        public void updateItem(Locale locale, boolean empty) {
            super.updateItem(locale, empty);
            if (empty) {
                setText(null);
            } else {
                setText(locale.getDisplayLanguage(locale));
            }
        }
    }

    public static class ObservableResourceFactory {

        private final ObjectProperty<ResourceBundle> resources = new SimpleObjectProperty<>();

        public ObjectProperty<ResourceBundle> resourcesProperty() {
            return resources;
        }

        public final ResourceBundle getResources() {
            return resourcesProperty().get();
        }

        public final void setResources(ResourceBundle resources) {
            resourcesProperty().set(resources);
        }

        public StringBinding getStringBinding(String key) {
            return new StringBinding() {
                {
                    bind(resourcesProperty());
                }

                @Override
                public String computeValue() {
                    return getResources().getString(key);
                }
            };
        }

    }

    public static class Resources extends ListResourceBundle {

        @Override
        protected Object[][] getContents() {
            return new Object[][]{
                    {"greeting", "Привет"}
            };
        }

    }

    public static class Resources_fr extends ListResourceBundle {

        @Override
        protected Object[][] getContents() {
            return new Object[][]{
                    {"greeting", "Bonjour"}
            };
        }

    }

    public static class Resources_en extends ListResourceBundle {

        @Override
        protected Object[][] getContents() {
            return new Object[][]{
                    {"greeting", "Hello"}
            };
        }

    }
}