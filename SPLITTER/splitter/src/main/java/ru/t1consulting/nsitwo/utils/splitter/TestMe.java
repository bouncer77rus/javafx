package ru.t1consulting.nsitwo.utils.splitter;

import java.util.Arrays;
import java.util.Locale;

public class TestMe {

    public static void main(String[] args) {
        System.out.println(Arrays.toString(Locale.getAvailableLocales()));
        System.out.println(Locale.getDefault());
        Locale rus = new Locale("ru", "RU");
        System.out.println(rus);
    }
}
