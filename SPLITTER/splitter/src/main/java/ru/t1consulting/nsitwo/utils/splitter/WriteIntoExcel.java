package ru.t1consulting.nsitwo.utils.splitter;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.FileUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.NumberToTextConverter;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.List;
import java.util.*;

public class WriteIntoExcel {

    public static void changeFileEncoding(String xmlFile, String resultXmlFile) {

        String line;

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(xmlFile), "WINDOWS-1251"));
             BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(resultXmlFile)))) {

            while ((line = reader.readLine()) != null) {

                writer.write(line);
                writer.newLine();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Map<Integer, List<Object>> readCsvFile(String fileName) {

        Map<Integer, List<Object>> map = new HashMap<>();
        try (Reader in = new FileReader(fileName)) {
            //String csvDelimiter = ConfProperties.getProperty("CsvDelimiter");
            String csvDelimiter = ";";
            System.out.println("Config CsvDelimiter = " + csvDelimiter);
            char csvDelimiterChar = csvDelimiter.charAt(0);
            System.out.println("CsvDelimiter = " + csvDelimiterChar);
            Iterable<CSVRecord> records = CSVFormat.EXCEL.withDelimiter(csvDelimiterChar).parse(in);
            for (CSVRecord record : records) {
                List<Object> objectList = new ArrayList<>();
                for (String str : record.toList()) {
                    if (str.trim().isEmpty()) {
                        objectList.add(null);
                    } else {
                        objectList.add(str);
                    }
                }

                map.put((int) record.getRecordNumber() - 1, objectList);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return map;
    }

    public static Map<Integer, List<Object>> readExcelFile(String fileName) {

        File file = new File(fileName.trim());

        if (!file.exists()) {
            System.out.println("Файла с по пути: " + fileName + " не обнаружено!");
            return null;
        }

        Map<Integer, List<Object>> data = null;

        try (FileInputStream fis = new FileInputStream(file); XSSFWorkbook workbook = new XSSFWorkbook(file)) {
            Sheet sheet = workbook.getSheetAt(1);
            data = processSheet(sheet);
        } catch (IOException e) {
            System.out.println("Ошибка при чтении файла " + fileName);
            e.printStackTrace();
        } catch (InvalidFormatException e) {
            System.out.println("Ошибка создания workbook на основе файла " + fileName);
            e.printStackTrace();
        }

        return data;
    }

    public static void deleteFilesInDir(String dirPath) {
        File dir = new File(dirPath);
        String[] str = {"xlsx"};
        Collection<File> files = FileUtils.listFiles(dir, str, true);
        for (File f : files) {
            f.delete();
        }
    }

    public static void splitExcelFile(String filePath, Map<Integer, List<Object>> data, int splitRow, boolean typeChecking) {


        System.out.println("data.size() = " + data.size()); // Включает заголовок таблицы => -1
        int splitNum = (data.size() - 1) % splitRow != 0 ? ((data.size() - 1) / splitRow) + 1 : ((data.size() - 1) / splitRow);
        System.out.println("splitNum = " + splitNum);

        File f = new File(filePath);
        String parent = f.getParent();
        System.out.println("Parent = " + parent);

        String fileName;
        String dirPath;
        if (filePath.endsWith(".csv")) {
            fileName = f.getName().substring(0, f.getName().length() - 4);
            // dirPath = parent + File.separator + fileName + "_csv_" + App.getSplitRow();
            dirPath = parent + File.separator + fileName + "_csv_" + "5000"; // TODO
        } else { // .xlsx
            fileName = f.getName().substring(0, f.getName().length() - 5);
            // dirPath = parent + File.separator + fileName + "_xlsx_" + App.getSplitRow();
            dirPath = parent + File.separator + fileName + "_xlsx_" + "5000"; // TODO
        }

        System.out.println("fileName = " + fileName);
        System.out.println("dirPath = " + dirPath);

        if (splitNum == 1) {
            System.out.println("Файл " + filePath + " не требуется делить!");
            return;
        } else {
            System.out.println("Создание директории!");

            if (new File(dirPath).exists()) {
                System.out.println("Директория " + dirPath + " уже существует!");
                deleteFilesInDir(dirPath);
                System.out.println("Были удалены все файлы в директории!");
            } else {
                try {
                    Files.createDirectory(Paths.get(dirPath));
                } catch (IOException e) {
                    System.out.println("Ошибка при создании директории для справочников");
                    e.printStackTrace();
                }
            }
        }

        String newFileName;
        for (int i = 0; i < splitNum; ++i) {
            newFileName = dirPath + File.separator + fileName + "_" + (i + 1) + "_out_of_" + splitNum + ".xlsx";
            int firstIndex = 0;
            int lastIndex = 0;

            if (i == 0) {
                firstIndex = 1;
                lastIndex = splitRow;
            } else if (i == (splitNum - 1)) {
                firstIndex = (splitRow * i) + 1;
                lastIndex = data.size() - 1;
                // System.out.println(newFileName + " : " + ((splitRow * i) + 1) + " - " + (data.size() + 2)); // +1 - нумерация с нуля в data, +1 - первая строка заголовки
            } else {
                firstIndex = (splitRow * i) + 1;
                lastIndex = splitRow * (i + 1);
                // System.out.println(newFileName + " : " + ((splitRow * i) + 1) + " - " + ((splitRow * (i + 1)) + 1));
            }
            System.out.println(newFileName + " : " + (firstIndex + 1) + " - " + (lastIndex + 1));

            if (typeChecking) {
                writeIntoExcel(newFileName, data, firstIndex, lastIndex);
            } else {
                writeIntoExcelWithoutCheckType(newFileName, data, firstIndex, lastIndex);
            }

        }

        // Toolkit.getDefaultToolkit().beep();
    }

    public static void writeIntoExcelWithoutCheckType(String fileName, Map<Integer, List<Object>> data, int firstIndex, int lastIndex) {

        File file = new File(fileName);

        try (FileOutputStream outputStream = new FileOutputStream(file); XSSFWorkbook workbook = new XSSFWorkbook()) {

            workbook.createSheet("Паспорт");
            Sheet sheet = workbook.createSheet("Данные");

            Row row = sheet.createRow(0);
            Cell cell;

            int rowNum = 0;

            // createHeader
            for (int i = 0; i < data.get(0).size(); ++i) {

                cell = row.createCell(i, CellType.STRING);
                // cell.setCellValue(BicRecord.columnNames.get(i));
                cell.setCellValue(data.get(0).get(i).toString());
            }

            // createCells
            for (int i = firstIndex; i <= lastIndex; ++i) {
                row = sheet.createRow(++rowNum);
                for (int j = 0; j < data.get(i).size(); ++j) {

                    if (Objects.isNull(data.get(i).get(j))) {
                        cell = row.createCell(j, CellType.BLANK);
                    } else {
                        cell = row.createCell(j, CellType.STRING);
                        cell.setCellValue(data.get(i).get(j).toString());
                    }
                }
            }
            workbook.write(outputStream);

        } catch (IOException e) {
            System.out.println("Ошибка при записи файла " + fileName);
        }
    }

    public static void writeIntoExcel(String fileName, Map<Integer, List<Object>> data, int firstIndex, int lastIndex) {

        File file = new File(fileName);

        try (FileOutputStream outputStream = new FileOutputStream(file); XSSFWorkbook workbook = new XSSFWorkbook()) {

            workbook.createSheet("Паспорт");
            Sheet sheet = workbook.createSheet("Данные");

            Row row = sheet.createRow(0);
            Cell cell;

            int rowNum = 0;

            // createHeader
            for (int i = 0; i < data.get(0).size(); ++i) {
                cell = row.createCell(i, CellType.STRING);
                // cell.setCellValue(BicRecord.columnNames.get(i));
                cell.setCellValue(data.get(0).get(i).toString());
            }

            // createCells
            for (int i = firstIndex; i <= lastIndex; ++i) {

                row = sheet.createRow(++rowNum);

                for (int j = 0; j < data.get(i).size(); ++j) {

                    // Отлавливает пустые ячейки, включая parentId
                    if (Objects.isNull(data.get(i).get(j))) {

                        // UI (ручной загрузчик Excel) не принимает строку для startDate
                        if (j == 3) {
                            cell = row.createCell(j, CellType.BLANK);
                        } else {
                            cell = row.createCell(j);
                            cell.setCellValue("");
                        }

                    } else if (j == 0 || j == 1) {
                        cell = row.createCell(j, CellType.NUMERIC);
                        cell.setCellValue(Integer.parseInt(data.get(i).get(j).toString()));

                    } else if (j == 3) {
                        cell = row.createCell(j, CellType.NUMERIC);
                        cell.setCellValue((LocalDateTime) data.get(i).get(j));
                    } else {
                        cell = row.createCell(j, CellType.STRING);
                        cell.setCellValue(data.get(i).get(j).toString());
                    }

                }
            }

            workbook.write(outputStream);
        } catch (IOException e) {
            System.out.println("Ошибка при записи файла " + fileName);
        } catch (NumberFormatException e) {
            System.out.println("Неверный формат");
            System.out.println("Ошибка при записи файла " + fileName);
        }
    }

    private static Map<Integer, List<Object>> processSheet(Sheet sheet) {
        System.out.println("Sheet: " + sheet.getSheetName());
        HashMap<Integer, List<Object>> data = new HashMap();
        Iterator<Row> iterator = sheet.rowIterator();
        for (int rowIndex = 0; iterator.hasNext(); rowIndex++) {
            Row row = iterator.next();
            processRow(data, rowIndex, row);
        }
        return data;
    }

    private static void processRow(HashMap<Integer, List<Object>> data, int rowIndex, Row row) {
        data.put(rowIndex, new ArrayList<>());
        for (int i = 0; i < row.getLastCellNum(); ++i) {
            processCell(row.getCell(i), data.get(rowIndex));
        }
    }

    private static void processCell(Cell cell, List<Object> dataRow) {

        if (Objects.isNull(cell)) {
            // dataRow.add(" ");
            dataRow.add(null);
            return;
        }

        switch (cell.getCellType()) {
            case STRING:
                dataRow.add(cell.getStringCellValue());
                break;
            case NUMERIC:
                if (DateUtil.isCellDateFormatted(cell)) {
                    dataRow.add(cell.getLocalDateTimeCellValue());
                } else {
                    dataRow.add(NumberToTextConverter.toText(cell.getNumericCellValue()));
                }
                break;
            case BOOLEAN:
                dataRow.add(cell.getBooleanCellValue());
                break;
            case FORMULA:
                dataRow.add(cell.getCellFormula());
                break;
            default:
                dataRow.add(" ");
        }
    }
}
